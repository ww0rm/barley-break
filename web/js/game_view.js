function GameView(fields, moves) {
    this.Move = {up: -4, down: 4, left: -1, right: 1};

    this.field = $("#game-field");
    this.playBtn = $("#play-btn");

    this.fields = fields;
    this.moves = moves;

    this.zeroPosition = 15;
}

GameView.prototype.start = function () {
    this.renderItems();
    this.play();
};

GameView.prototype.play = function () {
    var i = 0;
    var moveInterval = setInterval(function () {
        if (i < this.moves.length) {
            this.move(parseInt(this.moves[i]));
            i++;

            if (this.isCompleted()) {
                this.field.addClass("solved");
            }
        } else {
            clearInterval(moveInterval);
        }
    }.bind(this), 300);
};

GameView.prototype.renderItems = function () {
    this.field.empty();

    this.fields.forEach(function (i) {
        this.renderItem(i);
    }, this);
};

GameView.prototype.renderItem = function (item) {
    var div = $("<div></div>").addClass("game-field-item");
    if (item == 0)
        div.addClass("empty");
    else
        div.html(item);

    this.field.append(div);
};

GameView.prototype.move = function (direction) {
    var index = this.zeroPosition + direction;
    if (!this.fields[index]) return false;

    if (direction === this.Move.left || direction === this.Move.right)
        if (Math.floor(this.zeroPosition / 4) !== Math.floor(index / 4))
            return false;

    this.fields.swap(index, this.zeroPosition);
    this.zeroPosition = index;

    this.renderItems();
    return true;
};

GameView.prototype.isCompleted = function () {
    return !this.fields.some(function(item, i) {
        return item > 0 && item - 1 !== i;
    });
};