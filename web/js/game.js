function Game(game_id, fields) {
    this.Move = {up: -4, down: 4, left: -1, right: 1};

    this.field = $("#game-field");
    this.timer = $("#timer");

    this.game_id = game_id;
    this.fields = fields;

    this.time = 0;
    this.moves = [];
    this.timerStart = false;
    this.timerInterval = null;
    this.zeroPosition = 15;
}

Game.prototype.start = function () {
    this.bindEvents();
    this.renderItems();
};

Game.prototype.bindEvents = function () {
    $(document).on('keydown', function(e) {
        var direction = this.Move[{39: 'left', 37: 'right', 40: 'up', 38: 'down'}[e.keyCode]];
        if (this.move(direction)) {
            this.renderItems();

            if (this.isCompleted()) {
                this.field.addClass("solved");
                this.sendEnd();

                $(document).off('keydown');
                clearInterval(this.timerInterval);
            }
        }
    }.bind(this));

    this.timerInterval = setInterval(function () {
        if(this.timerStart) {
            this.time++;
            this.timer.html(this.time.toString().toTime())
        }
    }.bind(this), 1000);
};

Game.prototype.renderItems = function () {
    this.field.empty();

    this.fields.forEach(function (i) {
        this.renderItem(i);
    }, this);
};

Game.prototype.renderItem = function (item) {
    var div = $("<div></div>").addClass("game-field-item");
    if (item == 0)
        div.addClass("empty");
    else
        div.html(item);

    this.field.append(div);
};

Game.prototype.move = function (direction) {
    this.timerStart = true;

    var index = this.zeroPosition + direction;
    if (!this.fields[index]) return false;

    if (direction === this.Move.left || direction === this.Move.right)
        if (Math.floor(this.zeroPosition / 4) !== Math.floor(index / 4))
            return false;

    this.fields.swap(index, this.zeroPosition);
    this.zeroPosition = index;
    this.moves.push(parseInt(direction));

    return true;
};

Game.prototype.isCompleted = function () {
    return !this.fields.some(function(item, i) {
        return item > 0 && item - 1 !== i;
    });
};

Game.prototype.sendEnd = function () {
    $.post("/api/game/end", {game_id: this.game_id, moves: this.moves, time: this.time}, function(data) {
        console.log("game tracked");
    });
};