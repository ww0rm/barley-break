<?php

namespace AppBundle\Repository;
use AppBundle\Entity\Game;
use AppBundle\Entity\User;
use Doctrine\ORM\Mapping\OrderBy;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * GameRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class GameRepository extends \Doctrine\ORM\EntityRepository
{
    public function setAllEnded(User $user) {
        return $this->createQueryBuilder('t')
            ->update()
            ->set('t.endDate', ':endDate')
            ->set('t.status', ':newStatus')
            ->where('t.user = :user AND t.status = :status')
            ->setParameter('endDate', new \DateTime())
            ->setParameter('user', $user)
            ->setParameter('status', Game::STATUS_NEW)
            ->setParameter('newStatus', Game::STATUS_FAILED)
            ->getQuery()->getSingleScalarResult();
    }

    public function getLastUserGames(User $user) {
        return $this->findBy([
            'user' => $user,
            'status' => Game::STATUS_DONE
        ], ['endDate' => 'desc'], 10);
    }

    public function getTopByTime() {
        return $this->createQueryBuilder('t')
            ->where('t.status = :status')
            ->orderBy("t.usedTime", "asc")
            ->setParameter('status', Game::STATUS_DONE)
            ->getQuery()->execute();
    }

    public function getTopByMoves() {
        return $this->createQueryBuilder('t')
            ->where('t.status = :status')
            ->orderBy("t.movesCount", "asc")
            ->setParameter('status', Game::STATUS_DONE)
            ->getQuery()->execute();
    }
}
