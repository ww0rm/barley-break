<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Game
 *
 * @ORM\Table(name="game")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GameRepository")
 */
class Game
{
    const STATUS_NEW = 1;
    const STATUS_FAILED = 2;
    const STATUS_DONE = 3;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime")
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @var array
     *
     * @ORM\Column(name="fields", type="json_array")
     */
    private $fields = [];


    /**
     * @var array
     *
     * @ORM\Column(name="moves", type="json_array")
     */
    private $moves = [];

    /**
     * @var int
     *
     * @ORM\Column(name="moves_count", type="integer")
     */
    private $movesCount = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="used_time", type="integer")
     */
    private $usedTime = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status = self::STATUS_NEW;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return Game
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return Game
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set userId
     *
     * @param User $userId
     *
     * @return Game
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get userId
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set fields
     *
     * @param array $fields
     *
     * @return Game
     */
    public function setFields($fields)
    {
        $this->fields = $fields;

        return $this;
    }

    /**
     * Get fields
     *
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Set movesCount
     *
     * @param integer $movesCount
     *
     * @return Game
     */
    public function setMovesCount($movesCount)
    {
        $this->movesCount = $movesCount;

        return $this;
    }

    /**
     * Get movesCount
     *
     * @return int
     */
    public function getMovesCount()
    {
        return $this->movesCount;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getUsedTime()
    {
        return $this->usedTime;
    }

    /**
     * @param int $usedTime
     */
    public function setUsedTime($usedTime)
    {
        $this->usedTime = $usedTime;
    }

    /**
     * @return array
     */
    public function getMoves()
    {
        return $this->moves;
    }

    /**
     * @param array $moves
     */
    public function setMoves($moves)
    {
        $this->moves = $moves;
    }

    public function getFieldsInString() {
        return json_encode($this->fields);
    }

    public function getMovesInString() {
//        print_r($this->moves);
        return json_encode($this->moves);
    }
}

