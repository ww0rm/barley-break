<?php
/**
 * Created by PhpStorm.
 * User: ww0rm
 * Date: 2/8/17
 * Time: 13:53
 */

namespace AppBundle\Utils;


class FieldsGenerator
{
    public static function generate() {
        $array = range(1, 15);
        shuffle($array);
        $array[15] = 0;

        return $array;
    }
}