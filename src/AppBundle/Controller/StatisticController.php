<?php
/**
 * Created by PhpStorm.
 * User: ww0rm
 * Date: 2/8/17
 * Time: 23:52
 */

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class StatisticController extends Controller
{
    /**
     * @Route("/statistic", name="statistic")
     */
    public function indexAction()
    {
        $games = $this->getDoctrine()->getRepository('AppBundle:Game')->getLastUserGames($this->getUser());
        return $this->render('statistic/index.html.twig', [
            'games' => $games
        ]);
    }
}