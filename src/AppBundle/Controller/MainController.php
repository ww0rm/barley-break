<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MainController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $timeTop = $this->getDoctrine()->getRepository('AppBundle:Game')->getTopByTime();
        $movesTop = $this->getDoctrine()->getRepository('AppBundle:Game')->getTopByMoves();
        return $this->render('default/homepage.html.twig', [
            'timeTop' => $timeTop,
            'movesTop' => $movesTop
        ]);
    }
}
