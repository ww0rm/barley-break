<?php
/**
 * Created by PhpStorm.
 * User: ww0rm
 * Date: 2/8/17
 * Time: 13:35
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Game;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/api")
 */
class ApiController extends Controller
{
    /**
     * @Route("/game/end")
     * @Method("POST")
     */
    public function endGameAction(Request $request)
    {
        $game_id = $request->get('game_id');
        $moves = $request->get('moves');
        $time = $request->get('time');

        $game = $this->getDoctrine()->getRepository('AppBundle:Game')->find($game_id);
        if(is_null($game)) {
            return JsonResponse::create("", Response::HTTP_NOT_FOUND);
        }

        $game->setMovesCount(sizeof($moves));
        $game->setMoves($moves);
        $game->setUsedTime($time);
        $game->setEndDate(new \DateTime());
        $game->setStatus(Game::STATUS_DONE);

        $em = $this->getDoctrine()->getManager();
        $em->persist($game);
        $em->flush();

        return JsonResponse::create(['status' => 'ok']);
    }
}