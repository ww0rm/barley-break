<?php
/**
 * Created by PhpStorm.
 * User: ww0rm
 * Date: 2/8/17
 * Time: 23:28
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Game;
use AppBundle\Utils\FieldsGenerator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/game")
 */
class GameController extends Controller
{
    /**
     * @Route("/", name="game")
     */
    public function indexAction()
    {
        $this->getDoctrine()->getRepository('AppBundle:Game')->setAllEnded($this->getUser());

        $game = new Game();
        $game->setUser($this->getUser());
        $game->setStartDate(new \DateTime());
        $game->setFields(FieldsGenerator::generate());

        $em = $this->getDoctrine()->getManager();
        $em->persist($game);
        $em->flush();

        return $this->render('default/game.html.twig', [
            'game' => $game
        ]);
    }

    /**
     * @Route("/{id}/view", name="game_view")
     * @param int $id
     * @return int|Response
     */
    public function gameViewAction(int $id)
    {
        $game = $this->getDoctrine()->getRepository('AppBundle:Game')->find($id);
        if(is_null($game)) {
            return Response::HTTP_NOT_FOUND;
        }

        return $this->render('default/game_view.html.twig', [
            'game' => $game
        ]);
    }
}